import {ClassType} from '@dkx/types-class';

import {
	METADATA_COLUMNS,
	METADATA_ENTITY,
	METADATA_ID,
	METADATA_RELATIONSHIPS,
	METADATA_SELF_LINK, METADATA_SUPER_ENTITY
} from '../decorators';
import {
	EntityColumnMetadata,
	EntityMetadata, EntityRelationshipMetadata,
} from '../metadata';


export class DecoratorMetadataLoader
{
	public getEntityMetadata<T>(entityClass: ClassType<T>): EntityMetadata|undefined
	{
		const metadata: EntityMetadata = Reflect.getMetadata(METADATA_ENTITY, entityClass);
		if (typeof metadata === 'undefined') {
			return;
		}

		const id: string = Reflect.getMetadata(METADATA_ID, entityClass);
		if (typeof id !== 'undefined') {
			metadata.id = id;
		}

		const selfLink: string = Reflect.getMetadata(METADATA_SELF_LINK, entityClass);
		if (typeof selfLink !== 'undefined') {
			metadata.selfLink = selfLink;
		}

		const columnsList = this.getListOfPrototypesMetadata<Array<EntityColumnMetadata>>(entityClass, METADATA_COLUMNS);
		const columns = columnsList.reduce((result, columns) => {
			return [...result, ...columns];
		}, []);

		if (columns.length > 0) {
			metadata.columns = columns;
		}

		const relationshipsList = this.getListOfPrototypesMetadata<Array<EntityRelationshipMetadata<any>>>(entityClass, METADATA_RELATIONSHIPS);
		const relationships = relationshipsList.reduce((result, relationships) => {
			return [...result, ...relationships];
		}, []);

		if (relationships.length > 0) {
			metadata.relationships = relationships;
		}

		return metadata;
	}

	private getListOfPrototypesMetadata<T>(entityClass: ClassType<any>, name: string): Array<T>
	{
		return this.getRecursiveMetadata(entityClass, METADATA_ENTITY, name);
	}

	private getRecursiveMetadata<T>(entityClass: ClassType<any>, classMetadataTestName: string, name: string): Array<T>
	{
		const isValid = typeof Reflect.getOwnMetadata(classMetadataTestName, entityClass) !== 'undefined';
		if (!isValid) {
			return [];
		}

		const list: Array<T> = [];
		const data = Reflect.getOwnMetadata(name, entityClass);
		if (typeof data !== 'undefined') {
			list.push(data);
		}

		return [
			...list,
			...this.getRecursiveMetadata<T>(Object.getPrototypeOf(entityClass), METADATA_SUPER_ENTITY, name),
		];
	}
}

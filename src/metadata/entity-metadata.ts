import {ClassType} from '@dkx/types-class';


export declare type EntityColumnTransformer = (data: any) => any;

export declare interface EntityColumnMetadata
{
	name: string,
	property: string,
	transformers: Array<EntityColumnTransformer>,
}

export declare interface EntityRelationshipMetadata<T>
{
	entity: ClassType<T>|Promise<ClassType<T>>,
	name: string,
	property: string,
}

export declare interface EntityMetadata
{
	id: string,
	type: string|null,
	selfLink: string|null,
	columns: Array<EntityColumnMetadata>,
	relationships: Array<EntityRelationshipMetadata<any>>,
}

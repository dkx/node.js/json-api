export const METADATA_ENTITY: string = 'json_api:entity';
export const METADATA_SUPER_ENTITY: string = 'json_api:super_entity';
export const METADATA_ID: string = 'json_api:id';
export const METADATA_SELF_LINK: string = 'json_api:self_link';
export const METADATA_COLUMNS: string = 'json_api:columns';
export const METADATA_RELATIONSHIPS: string = 'json_api:relationships';

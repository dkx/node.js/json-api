import {ClassType} from '@dkx/types-class';

import {METADATA_ENTITY, METADATA_SUPER_ENTITY, METADATA_ID, METADATA_COLUMNS, METADATA_RELATIONSHIPS, METADATA_SELF_LINK} from './types';
import {EntityColumnMetadata, EntityColumnTransformer, EntityMetadata, EntityRelationshipMetadata} from '../metadata';


const DEFAULT_ID_PROPERTY: string = 'id';

export declare interface EntityOptions
{
	type?: string,
}

export declare interface EntityColumnOptions
{
	name?: string,
	transformers?: Array<EntityColumnTransformer>,
}

export declare interface EntityRelationOptions
{
	entity: ClassType<any>|Promise<ClassType<any>>,
	name?: string,
}

export function SuperEntity(): ClassDecorator
{
	return function (target: Function): void
	{
		Reflect.defineMetadata(METADATA_SUPER_ENTITY, true, target);
	};
}

export function Entity(options: EntityOptions = {}): ClassDecorator
{
	return function(target: Function): void
	{
		const metadata: EntityMetadata = {
			type: typeof options.type === 'undefined' ? null : options.type,
			id: DEFAULT_ID_PROPERTY,
			columns: [],
			relationships: [],
			selfLink: null,
		};

		Reflect.defineMetadata(METADATA_ENTITY, metadata, target);
	}
}

export function Id(): PropertyDecorator
{
	return function(target: Object, property: string|symbol): void
	{
		Reflect.defineMetadata(METADATA_ID, property, target.constructor);
	}
}

export function SelfLink(): PropertyDecorator
{
	return function(target: Object, property: string|symbol): void
	{
		Reflect.defineMetadata(METADATA_SELF_LINK, property, target.constructor);
	}
}

export function Column(options: EntityColumnOptions = {}): PropertyDecorator
{
	return function(target: Object, property: string|symbol): void
	{
		if (typeof property !== 'string') {
			throw new Error('Not supported');
		}

		const columns: Array<EntityColumnMetadata> = Reflect.getOwnMetadata(METADATA_COLUMNS, target.constructor) || [];

		columns.push({
			name: typeof options.name === 'undefined' ? property : options.name,
			transformers: typeof options.transformers === 'undefined' ? [] : options.transformers,
			property,
		});

		Reflect.defineMetadata(METADATA_COLUMNS, columns, target.constructor);
	}
}

export function Relationship(options: EntityRelationOptions): PropertyDecorator
{
	return function(target: Object, property: string|symbol): void
	{
		if (typeof property !== 'string') {
			throw new Error('Not supported');
		}

		const relationships: Array<EntityRelationshipMetadata<any>> = Reflect.getOwnMetadata(METADATA_RELATIONSHIPS, target.constructor) || [];

		relationships.push({
			entity: options.entity,
			name: typeof options.name === 'undefined' ? property : options.name,
			property,
		});

		Reflect.defineMetadata(METADATA_RELATIONSHIPS, relationships, target.constructor);
	}
}

export interface HttpRequestInterface
{
	readonly method: string;
	readonly url: string;
	readonly body?: any;
}

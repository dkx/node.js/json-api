import {HttpRequestInterface} from '../http-request-interface';
import {HttpResponseInterface} from '../../response';


export interface HttpRequestHandlerInterface<TData>
{
	handle(request: HttpRequestInterface): Promise<HttpResponseInterface<TData>>;
}

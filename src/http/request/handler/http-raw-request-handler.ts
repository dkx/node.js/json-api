import {HttpRequestHandlerInterface} from './http-request-handler-interface';
import {FetchInterface} from '../../../fetch';
import {HttpRequestInterface} from '../http-request-interface';
import {HttpResponse, HttpResponseInterface} from '../../response';
import {MiddlewareHandlerInterface} from '../../../middlewares';


export class HttpRawRequestHandler<TData> implements HttpRequestHandlerInterface<TData>, MiddlewareHandlerInterface<TData>
{
	constructor(
		private readonly fetch: FetchInterface,
	) {}

	public async handle(request: HttpRequestInterface): Promise<HttpResponseInterface<TData>>
	{
		return new HttpResponse(await this.fetch.request<TData>(request));
	}
}

import {JsonApiDocument} from '@dkx/types-json-api';
import {ClassType} from '@dkx/types-class';

import {HttpRequestHandlerInterface} from './http-request-handler-interface';
import {HttpResponse, HttpResponseInterface} from '../../response';
import {HttpRequestInterface} from '../http-request-interface';
import {FetchInterface} from '../../../fetch';
import {Mapper} from '../../../mapping';
import {MiddlewareHandlerInterface} from '../../../middlewares';


export class HttpItemRequestHandler<TEntity> implements HttpRequestHandlerInterface<TEntity|undefined>, MiddlewareHandlerInterface<TEntity|undefined>
{
	constructor(
		private readonly fetch: FetchInterface,
		private readonly mapper: Mapper,
		private readonly mapTo: ClassType<TEntity>,
	) {}

	public async handle(request: HttpRequestInterface): Promise<HttpResponseInterface<TEntity|undefined>>
	{
		const data = await this.fetch.request<JsonApiDocument>(request);

		if (typeof data === 'undefined') {
			return new HttpResponse<TEntity>();
		}

		return new HttpResponse<TEntity>(
			await this.mapper.mapItem<TEntity>(this.mapTo, data),
			data.meta,
		);
	}
}

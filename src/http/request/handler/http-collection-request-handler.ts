import {ClassType} from '@dkx/types-class';
import {JsonApiDocument} from '@dkx/types-json-api';

import {HttpRequestHandlerInterface} from './http-request-handler-interface';
import {HttpRequestInterface} from '../http-request-interface';
import {HttpResponse, HttpResponseInterface} from '../../response';
import {FetchInterface} from '../../../fetch';
import {Mapper} from '../../../mapping';
import {MiddlewareHandlerInterface} from '../../../middlewares';


export class HttpCollectionRequestHandler<TEntity> implements HttpRequestHandlerInterface<Array<TEntity>>, MiddlewareHandlerInterface<Array<TEntity>>
{
	constructor(
		private readonly fetch: FetchInterface,
		private readonly mapper: Mapper,
		private readonly mapTo: ClassType<TEntity>,
	) {}

	public async handle(request: HttpRequestInterface): Promise<HttpResponseInterface<Array<TEntity>>>
	{
		const data = await this.fetch.request<JsonApiDocument>(request);

		if (typeof data === 'undefined') {
			return new HttpResponse<Array<TEntity>>();
		}

		return new HttpResponse<Array<TEntity>>(
			await this.mapper.mapCollection<TEntity>(this.mapTo, data),
			data.meta,
		);
	}
}

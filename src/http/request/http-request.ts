import {HttpRequestInterface} from './http-request-interface';


export class HttpRequest implements HttpRequestInterface
{
	constructor(
		public readonly method: string,
		public readonly url: string,
		public readonly body?: any,
	) {}
}

import {JsonApiMeta} from '@dkx/types-json-api';

import {HttpResponseInterface} from './http-response-interface';


export class HttpResponse<TData> implements HttpResponseInterface<TData>
{
	constructor(
		public readonly data?: TData,
		public readonly meta?: JsonApiMeta,
	) {}
}

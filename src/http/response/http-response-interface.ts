import {JsonApiMeta} from '@dkx/types-json-api';


export interface HttpResponseInterface<TData>
{
	readonly data?: TData;
	readonly meta?: JsonApiMeta,
}

import {HttpRequestInterface} from '../http';


export interface FetchInterface
{
	request<TResult>(request: HttpRequestInterface): Promise<TResult>;
}

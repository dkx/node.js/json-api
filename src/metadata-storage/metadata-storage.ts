import {ClassType} from '@dkx/types-class';

import {EntityMetadata, EntityRelationshipMetadata} from '../metadata';
import {DecoratorMetadataLoader} from '../metadata-loaders';


export class MetadataStorage
{
	private metadata: WeakMap<ClassType<any>, EntityMetadata> = new WeakMap;

	constructor(
		private loader: DecoratorMetadataLoader,
	) {}

	public getEntityMetadata<T>(entityClass: ClassType<T>): EntityMetadata
	{
		if (!this.metadata.has(entityClass)) {
			this.metadata.set(entityClass, this.loadMetadata(entityClass));
		}

		return <EntityMetadata>this.metadata.get(entityClass);
	}

	public extractIdProperty(metadata: EntityMetadata): string
	{
		return metadata.id;
	}

	public extractRelationship(metadata: EntityMetadata, name: string): EntityRelationshipMetadata<any>|undefined
	{
		return metadata.relationships.find((relationship: EntityRelationshipMetadata<any>) => relationship.name === name);
	}

	private loadMetadata<T>(entityClass: ClassType<T>): EntityMetadata
	{
		const metadata = this.loader.getEntityMetadata(entityClass);

		if (typeof metadata === 'undefined') {
			throw new Error(`Missing entity metadata for class ${entityClass.name}`);
		}

		return metadata;
	}
}

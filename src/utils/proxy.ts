import {ClassType} from '@dkx/types-class';


export function createProxy<TEntity>(type: ClassType<TEntity>): TEntity
{
	class JsonApiEntityProxy
	{
		constructor() {}
	}

	Object.setPrototypeOf(JsonApiEntityProxy.prototype, type.prototype);
	Object.setPrototypeOf(JsonApiEntityProxy, type);

	return <TEntity>(new JsonApiEntityProxy());
}

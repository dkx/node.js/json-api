import {ApiRequestOptions, RequestUrlParameters} from '../api-client';


export function createUrl(baseUrl: string, url: string, options: ApiRequestOptions = {}): string
{
	const parameters = typeof options.parameters === 'undefined' ? {} : {...options.parameters};

	if (typeof options.includes !== 'undefined' && options.includes.length > 0) {
		parameters['include'] = options.includes.join(',');
	}

	const targetUrl = url.charAt(0) === '@' ?
		url.substring(1) :
		`${baseUrl}${url}`;

	return buildUrl(targetUrl, parameters);
}

function buildUrl(url: string, parameters: RequestUrlParameters): string
{
	let params: Array<string> = [];

	for (let name in parameters) {
		if (parameters.hasOwnProperty(name)) {
			params.push(`${name}=${encodeURIComponent(parameters[name])}`);
		}
	}

	if (params.length > 0) {
		const join = url.indexOf('?') >= 0 ? '&' : '?';
		url += `${join}${params.join('&')}`;
	}

	return url;
}

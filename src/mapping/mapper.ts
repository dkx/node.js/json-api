import {ClassType} from '@dkx/types-class';
import {DocumentWalker} from '@dkx/json-api-walker';
import {JsonApiDocument, JsonApiRelationship, JsonApiResource} from '@dkx/types-json-api';

import {createProxy} from '../utils';
import {MetadataStorage} from '../metadata-storage';
import {EntityColumnMetadata, EntityMetadata, EntityRelationshipMetadata} from '../metadata';


export class Mapper
{
	constructor(
		private metadataStorage: MetadataStorage,
	) {}

	public mapItem<TEntity>(entityType: ClassType<TEntity>, document: JsonApiDocument): Promise<TEntity|undefined>
	{
		const walker = Mapper.prepareWalker(document);

		if (walker.isNull()) {
			return Promise.resolve(undefined);
		}

		if (!walker.isItem()) {
			throw new Error(`Expected item response`);
		}

		return this.mapItemData(
			entityType,
			this.metadataStorage.getEntityMetadata(entityType),
			walker,
			walker.getItem(),
		);
	}

	public mapCollection<TEntity>(entityType: ClassType<TEntity>, document: JsonApiDocument): Promise<Array<TEntity>>
	{
		const walker = Mapper.prepareWalker(document);

		if (walker.isNull()) {
			return Promise.resolve([]);
		}

		if (!walker.isCollection()) {
			throw new Error(`Expected collection response`);
		}

		return this.mapCollectionData(
			entityType,
			this.metadataStorage.getEntityMetadata(entityType),
			walker,
			walker.getCollection(),
		);
	}

	private static prepareWalker(document: JsonApiDocument): DocumentWalker
	{
		const walker = new DocumentWalker(document);

		if (walker.isEmpty()) {
			throw new Error(`Missing response data`);
		}

		return walker;
	}

	private async mapCollectionData<TEntity>(entityType: ClassType<TEntity>, metadata: EntityMetadata, walker: DocumentWalker, resources: Array<JsonApiResource>): Promise<Array<TEntity>>
	{
		const result: Array<TEntity> = [];

		for (let item of resources) {
			result.push(await this.mapItemData(entityType, metadata, walker, item));
		}

		return result;
	}

	private async mapItemData<TEntity>(entityType: ClassType<TEntity>, metadata: EntityMetadata, walker: DocumentWalker, resource: JsonApiResource): Promise<TEntity>
	{
		if (metadata.type !== null && metadata.type !== resource.type) {
			throw new Error(`Can not map "${resource.type}" data into entity ${entityType.name}. Entity can receive only resources of type "${metadata.type}"`);
		}

		const entity = createProxy(entityType);
		const idProperty = this.metadataStorage.extractIdProperty(metadata);

		entity[idProperty] = resource.id;

		for (let column of metadata.columns) {
			this.injectColumn(walker, entity, resource, column);
		}

		if (typeof resource.relationships !== 'undefined') {
			for (let name in resource.relationships) {
				if (resource.relationships.hasOwnProperty(name)) {
					let relationship: JsonApiRelationship = resource.relationships[name];
					let relationshipMetadata = this.metadataStorage.extractRelationship(metadata, name);

					if (typeof relationshipMetadata !== 'undefined') {
						entity[relationshipMetadata.property] = await this.lookupRelationship(walker, relationship, entityType, relationshipMetadata);
					}
				}
			}
		}

		if (typeof metadata.selfLink !== 'undefined' && metadata.selfLink !== null && typeof resource.links !== 'undefined' && resource.links.self !== 'undefined') {
			entity[metadata.selfLink] = resource.links.self;
		}

		return entity;
	}

	private async lookupRelationship<T>(walker: DocumentWalker, relationship: JsonApiRelationship, entityType: ClassType<T>, metadata: EntityRelationshipMetadata<any>): Promise<null|T|Array<T>>
	{
		const included = walker.getIncludedRelationship(relationship);

		if (included === null) {
			return null;
		}

		const entity = await Mapper.getEntityFromRelationshipMetadata<any>(metadata);

		if (typeof entity === 'undefined') {
			throw new Error(`Could not find relationship entity class for ${entityType.name}.${metadata.property}`);
		}

		const entityMetadata = this.metadataStorage.getEntityMetadata(entity);

		if (Array.isArray(included)) {
			return this.mapCollectionData(entity, entityMetadata, walker, included);
		} else {
			return this.mapItemData(entity, entityMetadata, walker, included);
		}
	}

	private static async getEntityFromRelationshipMetadata<T>(metadata: EntityRelationshipMetadata<any>): Promise<ClassType<T>>
	{
		if (metadata.entity instanceof Promise) {
			return metadata.entity;
		}

		return Promise.resolve(metadata.entity);
	}

	private injectColumn(walker: DocumentWalker, entity: any, resource: JsonApiResource, column: EntityColumnMetadata): void
	{
		if (column.name.indexOf('.') >= 0 || column.name.indexOf(':') >= 0) {
			return this.injectColumnFromRelationship(walker, entity, resource, column);
		}

		if (typeof resource.attributes === 'undefined') {
			return;
		}

		let attribute = resource.attributes[column.name];

		if (typeof attribute === 'undefined') {
			return;
		}

		for (let transformer of column.transformers) {
			attribute = transformer(attribute);
		}

		entity[column.property] = attribute;
	}

	private injectColumnFromRelationship(walker: DocumentWalker, entity: EntityColumnMetadata, resource: JsonApiResource, column: EntityColumnMetadata): void
	{
		const [columnName, relationshipColumn] = column.name.split(':');
		const columnPath = columnName.split('.');
		let currentResource: JsonApiResource|Array<JsonApiResource> = resource;

		for (let step of columnPath) {
			if (Array.isArray(currentResource)) {
				return;
			}

			if (typeof currentResource.relationships === 'undefined') {
				return;
			}

			const relationship = currentResource.relationships[step];

			if (typeof relationship === 'undefined') {
				return;
			}

			const foundResource = walker.getIncludedRelationship(relationship);
			if (!foundResource) {
				return;
			}

			currentResource = foundResource;
		}

		if (relationshipColumn === '@') {
			if (Array.isArray(currentResource)) {
				entity[column.property] = currentResource.map((item) => item.id);
			} else {
				entity[column.property] = currentResource.id;
			}
		} else {
			if (Array.isArray(currentResource)) {
				entity[column.property] = [];
				currentResource.forEach((item) => {
					if (typeof item.attributes !== 'undefined') {
						entity[column.property].push(item.attributes[relationshipColumn]);
					}
				});
			} else if (typeof currentResource.attributes !== 'undefined') {
				entity[column.property] = currentResource.attributes[relationshipColumn];
			}
		}
	}
}

import {ClassType} from '@dkx/types-class';

import {FetchInterface} from '../fetch';
import {Mapper} from '../mapping';
import {createUrl} from '../utils';
import {DecoratorMetadataLoader} from '../metadata-loaders';
import {MetadataStorage} from '../metadata-storage';
import {HttpCollectionRequestHandler, HttpItemRequestHandler, HttpRawRequestHandler, HttpRequestHandlerInterface, HttpRequestInterface, HttpResponseInterface} from '../http';
import {HttpRequest} from '../http/request';
import {MiddlewareDispatcher, MiddlewareInterface} from '../middlewares';


export declare interface ApiClientOptions
{
	baseUrl?: string,
}

export declare type ApiRequestIncludes = Array<string>;

export declare interface RequestUrlParameters
{
	[name: string]: string|number|boolean,
}

export declare interface ApiRequestOptions
{
	includes?: ApiRequestIncludes,
	parameters?: RequestUrlParameters,
}

export class ApiClient
{
	private readonly mapper: Mapper;

	private readonly baseUrl: string;

	private readonly middlewares: Array<MiddlewareInterface> = [];

	constructor(
		private readonly fetch: FetchInterface,
		options: ApiClientOptions = {},
	) {
		this.mapper = new Mapper(new MetadataStorage(new DecoratorMetadataLoader()));
		this.baseUrl = typeof options.baseUrl === 'undefined' ? '' : options.baseUrl;
	}

	public use(middleware: MiddlewareInterface): void
	{
		this.middlewares.push(middleware);
	}

	public getRaw<TResult>(url: string, options: ApiRequestOptions = {}): Promise<HttpResponseInterface<TResult>>
	{
		return this.dispatch<TResult>(
			this.createRequest('GET', url, options),
			this.createRawRequestHandler<TResult>(),
		);
	}

	public getOne<TEntity>(mapTo: ClassType<TEntity>, url: string, options: ApiRequestOptions = {}): Promise<HttpResponseInterface<TEntity|undefined>>
	{
		return this.dispatch<TEntity|undefined>(
			this.createRequest('GET', url, options),
			this.createItemRequestHandler<TEntity>(mapTo),
		);
	}

	public getMany<TEntity>(mapTo: ClassType<TEntity>, url: string, options: ApiRequestOptions = {}): Promise<HttpResponseInterface<Array<TEntity>>>
	{
		return this.dispatch<Array<TEntity>>(
			this.createRequest('GET', url, options),
			this.createCollectionRequestHandler<TEntity>(mapTo),
		);
	}

	public putRaw<TResult>(url: string, body: any, options: ApiRequestOptions = {}): Promise<HttpResponseInterface<TResult>>
	{
		return this.dispatch<TResult>(
			this.createRequest('PUT', url, options, body),
			this.createRawRequestHandler<TResult>(),
		);
	}

	public put<TEntity>(mapTo: ClassType<TEntity>, url: string, body: any, options: ApiRequestOptions = {}): Promise<HttpResponseInterface<TEntity|undefined>>
	{
		return this.dispatch<TEntity|undefined>(
			this.createRequest('PUT', url, options, body),
			this.createItemRequestHandler<TEntity>(mapTo),
		);
	}

	public postRaw<TResult>(url: string, body: any, options: ApiRequestOptions = {}): Promise<HttpResponseInterface<TResult>>
	{
		return this.dispatch<TResult>(
			this.createRequest('POST', url, options, body),
			this.createRawRequestHandler<TResult>(),
		);
	}

	public post<TEntity>(mapTo: ClassType<TEntity>, url: string, body: any, options: ApiRequestOptions = {}): Promise<HttpResponseInterface<TEntity|undefined>>
	{
		return this.dispatch<TEntity|undefined>(
			this.createRequest('POST', url, options, body),
			this.createItemRequestHandler<TEntity>(mapTo),
		);
	}

	public delete<TResult = void>(url: string, options: ApiRequestOptions = {}): Promise<HttpResponseInterface<TResult>>
	{
		return this.dispatch<TResult>(
			this.createRequest('DELETE', url, options),
			this.createRawRequestHandler<TResult>(),
		);
	}

	private dispatch<TData>(request: HttpRequestInterface, handler: HttpRequestHandlerInterface<TData>): Promise<HttpResponseInterface<TData>>
	{
		const middlewares = new MiddlewareDispatcher<TData>(handler);
		for (let i = this.middlewares.length - 1; i >= 0; i--) {
			middlewares.add(this.middlewares[i]);
		}

		return middlewares.handle(request);
	}

	private createRequest(method: string, url: string, options: ApiRequestOptions, body?: any): HttpRequestInterface
	{
		return new HttpRequest(method, createUrl(this.baseUrl, url, options), body);
	}

	private createRawRequestHandler<TResult>(): HttpRequestHandlerInterface<TResult>
	{
		return new HttpRawRequestHandler<TResult>(this.fetch);
	}

	private createItemRequestHandler<TEntity>(mapTo: ClassType<TEntity>): HttpRequestHandlerInterface<TEntity|undefined>
	{
		return new HttpItemRequestHandler(this.fetch, this.mapper, mapTo);
	}

	private createCollectionRequestHandler<TEntity>(mapTo: ClassType<TEntity>): HttpRequestHandlerInterface<Array<TEntity>>
	{
		return new HttpCollectionRequestHandler(this.fetch, this.mapper, mapTo);
	}
}

import {HttpRequestInterface} from '../http/request';
import {HttpResponseInterface} from '../http/response';


export interface MiddlewareHandlerInterface<TData>
{
	handle(request: HttpRequestInterface): Promise<HttpResponseInterface<TData>>;
}

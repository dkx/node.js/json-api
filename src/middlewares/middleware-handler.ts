import {MiddlewareInterface} from './middleware-interface';
import {HttpRequestInterface} from '../http/request';
import {HttpResponseInterface} from '../http/response';
import {MiddlewareHandlerInterface} from './middleware-handler-interface';


export class MiddlewareHandler<TData> implements MiddlewareHandlerInterface<TData>
{
	constructor(
		private readonly middleware: MiddlewareInterface,
		private readonly next: MiddlewareHandlerInterface<TData>,
	) {}

	public handle(request: HttpRequestInterface): Promise<HttpResponseInterface<TData>>
	{
		return this.middleware.invoke(request, this.next);
	}
}

import {HttpRequestInterface, HttpResponseInterface} from '../http';
import {MiddlewareHandlerInterface} from './middleware-handler-interface';


export interface MiddlewareInterface
{
	invoke<TData>(request: HttpRequestInterface, next: MiddlewareHandlerInterface<TData>): Promise<HttpResponseInterface<TData>>;
}

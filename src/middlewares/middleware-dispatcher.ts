import {MiddlewareInterface} from './middleware-interface';
import {HttpResponseInterface} from '../http/response';
import {MiddlewareHandler} from './middleware-handler';
import {HttpRequestInterface} from '../http/request';
import {MiddlewareHandlerInterface} from './middleware-handler-interface';


export class MiddlewareDispatcher<TData>
{
	constructor(
		private tip: MiddlewareHandlerInterface<TData>,
	) {}

	public add(middleware: MiddlewareInterface): void
	{
		this.tip = new MiddlewareHandler<TData>(middleware, this.tip);
	}

	public handle(request: HttpRequestInterface): Promise<HttpResponseInterface<TData>>
	{
		return this.tip.handle(request);
	}
}

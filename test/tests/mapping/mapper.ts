import 'reflect-metadata';

import {expect, use as chaiUse} from 'chai';
import * as chaiAsPromised from 'chai-as-promised';

import {Mapper, DecoratorMetadataLoader, MetadataStorage, Entity, Id, Column, Relationship, SelfLink} from '../../../src';


chaiUse(chaiAsPromised);


let mapper: Mapper;


describe('#Mapping/Mapper', () => {

	beforeEach(() => {
		mapper = new Mapper(new MetadataStorage(new DecoratorMetadataLoader()));
	});

	describe('mapItem()', () => {

		it('should map resource identifier', async () => {
			@Entity()
			class TestT
			{
				@Id() public entityId: string;
			}

			const entity = await mapper.mapItem(TestT, {
				data: {
					type: 'test',
					id: '10',
				},
			});

			expect(entity).to.be.an.instanceOf(TestT);
			expect(entity!.entityId).to.be.equal('10');
		});

		it('should map resource', async () => {
			@Entity()
			class Test
			{
				@Id() public readonly id: string;
				@Column({name: 'name'}) public readonly userName: string;
			}

			const entity = await mapper.mapItem(Test, {
				data: {
					type: 'test',
					id: '10',
					attributes: {
						name: 'John Doe',
					},
				},
			});

			expect(entity).to.be.an.instanceOf(Test);
			expect(entity!.id).to.be.equal('10');
			expect(entity!.userName).to.be.equal('John Doe');
		});

		it('should map resource with self link', async () => {
			@Entity()
			class Test
			{
				@Id() public readonly id: string;
				@SelfLink() public readonly link: string;
			}

			const entity = await mapper.mapItem(Test, {
				data: {
					type: 'test',
					id: '10',
					links: {
						self: 'http://localhost',
					},
				},
			});

			expect(entity).to.be.an.instanceOf(Test);
			expect(entity!.id).to.be.equal('10');
			expect(entity!.link).to.be.equal('http://localhost');
		});

		it('should map resource with relationships', async () => {
			@Entity()
			class TestC
			{
				@Id() public readonly id: string;
			}

			@Entity()
			class TestB
			{
				@Id() public readonly id: string;
				@Relationship({entity: TestC}) public readonly c: TestC;
			}

			@Entity()
			class TestA
			{
				@Id() public readonly id: string;
				@Relationship({entity: TestB}) public readonly b: TestB;
			}

			const entity = await mapper.mapItem(TestA, {
				data: {
					type: 'test-a',
					id: '1',
					relationships: {
						b: {
							data: {
								type: 'test-b',
								id: '2',
							},
						},
					},
				},
				included: [
					{
						type: 'test-b',
						id: '2',
						relationships: {
							c: {
								data: {
									type: 'test-c',
									id: '3',
								},
							},
						},
					},
					{
						type: 'test-c',
						id: '3',
					},
				],
			});

			expect(entity).to.be.an.instanceOf(TestA);
			expect(entity!.id).to.be.equal('1');
			expect(entity!.b).to.be.an.instanceOf(TestB);
			expect(entity!.b.id).to.be.equal('2');
			expect(entity!.b.c).to.be.an.instanceOf(TestC);
			expect(entity!.b.c.id).to.be.equal('3');
		});

		it('should map column with transformers', async () => {
			@Entity()
			class TestT
			{
				@Id() public id: string;

				@Column({
					transformers: [
						(data: string) => data + 'b',
						(data: string) => data + 'c',
					],
				})
				public name: string;
			}

			const entity = await mapper.mapItem<TestT>(TestT, {
				data: {
					type: 'test',
					id: '10',
					attributes: {
						name: 'a',
					},
				},
			});

			expect(entity).to.be.an.instanceOf(TestT);
			expect(entity!.id).to.be.equal('10');
			expect(entity!.name).to.be.equal('abc');
		});

		it('should throw an error if resource type and entity type are not matching', async () => {
			@Entity({
				type: 'test',
			})
			class Test {}

			await expect(mapper.mapItem(Test, {
				data: {
					type: 'user',
					id: '10',
				},
			})).to.be.rejectedWith(Error, 'Can not map "user" data into entity Test. Entity can receive only resources of type "test"');
		});

		it('should map matching type', async () => {
			@Entity({
				type: 'test',
			})
			class Test
			{
				@Id() public id: string;
			}

			const entity = await mapper.mapItem<Test>(Test, {
				data: {
					type: 'test',
					id: '10',
				},
			});

			expect(entity).to.be.an.instanceOf(Test);
			expect(entity!.id).to.be.equal('10');
		});

		it('should map relationship with dynamic import', async () => {
			@Entity()
			class TestB
			{
				@Id() public readonly id: string;
			}

			@Entity()
			class TestA
			{
				@Id() public readonly id: string;

				@Relationship({
					entity: Promise.resolve(TestB),
				})
				public readonly b: TestB;
			}

			const entity = await mapper.mapItem(TestA, {
				data: {
					type: 'test-a',
					id: '1',
					relationships: {
						b: {
							data: {
								type: 'test-b',
								id: '2',
							},
						},
					},
				},
				included: [
					{
						type: 'test-b',
						id: '2',
					},
				],
			});

			expect(entity).to.be.an.instanceOf(TestA);
			expect(entity!.id).to.be.equal('1');
			expect(entity!.b).to.be.an.instanceOf(TestB);
			expect(entity!.b.id).to.be.equal('2');
		});

		it('should map relationships into flat entity', async () => {
			@Entity()
			class Test
			{
				@Id() public readonly id: string;
				@Column({name: 'inner:@'}) public readonly innerId: string;
				@Column({name: 'inner:name'}) public readonly inner: string;
				@Column({name: 'inner.simple:@'}) public readonly innerSimpleId: string;
				@Column({name: 'inner.simple:name'}) public readonly innerSimple: string;
				@Column({name: 'inner.collection:@'}) public readonly innerCollectionIds: Array<string>;
				@Column({name: 'inner.collection:name'}) public readonly innerCollection: Array<string>;
			}

			const entity = await mapper.mapItem(Test, {
				data: {
					type: 'test',
					id: 'test-1',
					relationships: {
						inner: {
							data: {
								type: 'inner',
								id: 'inner-1',
							},
						},
					},
				},
				included: [
					{
						type: 'inner',
						id: 'inner-1',
						attributes: {
							name: 'INNER 1',
						},
						relationships: {
							simple: {
								data: {
									type: 'simple',
									id: 'simple-1',
								},
							},
							collection: {
								data: [
									{
										type: 'collection',
										id: 'collection-1',
									},
									{
										type: 'collection',
										id: 'collection-2',
									},
								],
							},
						},
					},
					{
						type: 'simple',
						id: 'simple-1',
						attributes: {
							name: 'SIMPLE 1',
						},
					},
					{
						type: 'collection',
						id: 'collection-1',
						attributes: {
							name: 'COLLECTION 1',
						},
					},
					{
						type: 'collection',
						id: 'collection-2',
						attributes: {
							name: 'COLLECTION 2',
						},
					},
				],
			});

			expect(entity!.id).to.be.equal('test-1');
			expect(entity!.innerId).to.be.equal('inner-1');
			expect(entity!.inner).to.be.equal('INNER 1');
			expect(entity!.innerSimpleId).to.be.equal('simple-1');
			expect(entity!.innerSimple).to.be.equal('SIMPLE 1');
			expect(entity!.innerCollectionIds).to.be.eql(['collection-1', 'collection-2']);
			expect(entity!.innerCollection).to.be.eql(['COLLECTION 1', 'COLLECTION 2']);
		});

	});

});

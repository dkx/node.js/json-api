import {expect} from 'chai';

import {MetadataStorage, DecoratorMetadataLoader, Entity} from '../../../src';
import {EntityMetadata} from '../../../src/metadata';


let metadataStorage: MetadataStorage;


describe('#MetadataStorage/MetadataStorage', () => {

	beforeEach(() => {
		metadataStorage = new MetadataStorage(new DecoratorMetadataLoader());
	});

	describe('getEntityMetadata()', () => {

		it('should throw an error when metadata is missing', () => {
			class TestClass {}

			expect(() => {
				metadataStorage.getEntityMetadata(TestClass);
			}).to.throw(Error, 'Missing entity metadata for class TestClass');
		});

		it('should get entity metadata', () => {
			@Entity()
			class TestClass {}

			const entityMetadata: EntityMetadata = {
				id: 'id',
				type: null,
				selfLink: null,
				columns: [],
				relationships: [],
			};

			expect(metadataStorage.getEntityMetadata(TestClass)).to.be.eql(entityMetadata);
			expect(metadataStorage.getEntityMetadata(TestClass)).to.be.eql(entityMetadata);	// test cached result
		});

	});

});

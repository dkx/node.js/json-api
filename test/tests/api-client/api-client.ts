import {expect} from 'chai';
import {JsonApiDocument} from '@dkx/types-json-api';
import {retryPromise} from '@dkx/retry-promise';

import {MockFetch} from '../../mocks';
import {
	ApiClient,
	Entity,
	Id,
	Column,
	HttpRequestInterface,
	MiddlewareInterface,
	HttpResponseInterface, MiddlewareHandlerInterface
} from '../../../src';
import {HttpResponse} from '../../../src/http/response';


let fetch: MockFetch;
let client: ApiClient;

describe('#ApiClient/ApiClient', () => {

	beforeEach(() => {
		fetch = new MockFetch;
		client = new ApiClient(fetch);
	});

	describe('get()', () => {

		it('should call getRaw', async () => {
			const data = {
				message: 'hello world',
			};

			fetch.addResponse('GET', '', data);

			const res = await client.getRaw('');

			expect(res.data).to.be.eql(data);
		});

		it('should call get with absolute url', async () => {
			const client = new ApiClient(fetch, {
				baseUrl: 'http://localhost:80',
			});

			fetch.onRequest((req: HttpRequestInterface) => {
				expect(req.url).to.be.equal('http://localhost:8080/v2/absolute');
			});

			await client.getRaw('@http://localhost:8080/v2/absolute');
		});

		it('should call get and return nothing', async () => {
			const res = await client.getRaw('');

			expect(res.data).to.be.eql(undefined);
		});

		it('should call get and map data to entity', async () => {
			@Entity()
			class User
			{
				@Id() public readonly id: string;
				@Column() public readonly name: string;
			}

			const doc: JsonApiDocument = {
				data: {
					type: 'user',
					id: '5',
					attributes: {
						name: 'John Doe',
					},
				},
			};

			fetch.addResponse('GET', '', doc);

			const user = await client.getOne<User>(User, '');

			expect(user.data).to.be.an.instanceOf(User);
			expect(user.data!.id).to.be.equal('5');
			expect(user.data!.name).to.be.equal('John Doe');
		});

		it('should pass meta data', async () => {
			@Entity()
			class User
			{
				@Id() public readonly id: string;
			}

			const doc: JsonApiDocument = {
				data: {
					type: 'user',
					id: '5',
				},
				meta: {
					message: 'hello world',
				},
			};

			fetch.addResponse('GET', '', doc);

			const user = await client.getOne<User>(User, '');

			expect(user.data).to.be.an.instanceOf(User);
			expect(user.data!.id).to.be.equal('5');
			expect(user.meta).to.be.eql({message: 'hello world'});
		});

		it('should parse simple includes list', async () => {
			fetch.onRequest((req: HttpRequestInterface) => {
				expect(req.url).to.be.equal('?include=author%2Cbook');
			});

			await client.getRaw('', {
				includes: ['author', 'book'],
			});
		});

		it('should call middlewares', async () => {
			const called: Array<string> = [];

			class MiddlewareA implements MiddlewareInterface
			{
				public invoke<TData>(request: HttpRequestInterface, next: MiddlewareHandlerInterface<TData>): Promise<HttpResponseInterface<TData>>
				{
					called.push('a');
					return next.handle(request);
				}
			}

			class MiddlewareB implements MiddlewareInterface
			{
				public invoke<TData>(request: HttpRequestInterface, next: MiddlewareHandlerInterface<TData>): Promise<HttpResponseInterface<TData>>
				{
					called.push('b');
					return next.handle(request);
				}
			}

			class MiddlewareC implements MiddlewareInterface
			{
				public invoke<TData>(request: HttpRequestInterface, next: MiddlewareHandlerInterface<TData>): Promise<HttpResponseInterface<TData>>
				{
					called.push('c');
					return next.handle(request);
				}
			}

			client.use(new MiddlewareA());
			client.use(new MiddlewareB());
			client.use(new MiddlewareC());

			await client.getRaw('');

			expect(called).to.be.eql(['a', 'b', 'c']);
		});

		it('should call middlewares in reverse order', async () => {
			const called: Array<string> = [];

			class MiddlewareA implements MiddlewareInterface
			{
				public invoke<TData>(request: HttpRequestInterface, next: MiddlewareHandlerInterface<TData>): Promise<HttpResponseInterface<TData>>
				{
					const result = next.handle(request);
					called.push('a');
					return result;
				}
			}

			class MiddlewareB implements MiddlewareInterface
			{
				public invoke<TData>(request: HttpRequestInterface, next: MiddlewareHandlerInterface<TData>): Promise<HttpResponseInterface<TData>>
				{
					const result = next.handle(request);
					called.push('b');
					return result;
				}
			}

			class MiddlewareC implements MiddlewareInterface
			{
				public invoke<TData>(request: HttpRequestInterface, next: MiddlewareHandlerInterface<TData>): Promise<HttpResponseInterface<TData>>
				{
					const result = next.handle(request);
					called.push('c');
					return result;
				}
			}

			client.use(new MiddlewareA());
			client.use(new MiddlewareB());
			client.use(new MiddlewareC());

			await client.getRaw('');

			expect(called).to.be.eql(['c', 'b', 'a']);
		});

		it('should not call next middlewares', async () => {
			const called: Array<string> = [];

			class MiddlewareA implements MiddlewareInterface
			{
				public invoke<TData>(request: HttpRequestInterface, next: MiddlewareHandlerInterface<TData>): Promise<HttpResponseInterface<TData>>
				{
					called.push('a');
					return Promise.resolve(new HttpResponse());
				}
			}

			class MiddlewareB implements MiddlewareInterface
			{
				public invoke<TData>(request: HttpRequestInterface, next: MiddlewareHandlerInterface<TData>): Promise<HttpResponseInterface<TData>>
				{
					called.push('b');
					return next.handle(request);
				}
			}

			client.use(new MiddlewareA());
			client.use(new MiddlewareB());

			await client.getRaw('');

			expect(called).to.be.eql(['a']);
		});

		it('should retry request on error', async () => {
			let retries = 0;

			class RetryMiddleware implements MiddlewareInterface
			{
				public invoke<TData>(request: HttpRequestInterface, next: MiddlewareHandlerInterface<TData>): Promise<HttpResponseInterface<TData>>
				{
					return retryPromise<HttpResponseInterface<TData>>((times: number) => {
						retries = times;
						return next.handle(request);
					}, {
						retries: 3,
					});
				}
			}

			fetch.onRequest(() => {
				if (retries < 3) {
					throw new Error();
				}
			});

			client.use(new RetryMiddleware());

			await client.getRaw('');

			expect(retries).to.be.equal(3);
		});

		it('should call another request on error and retry', async () => {
			let first = true;

			class RetryMiddleware implements MiddlewareInterface
			{
				public async invoke<TData>(request: HttpRequestInterface, next: MiddlewareHandlerInterface<TData>): Promise<HttpResponseInterface<TData>>
				{
					try {
						return await next.handle(request);
					} catch (e) {
						first = false;
						await client.getRaw('');
						return await next.handle(request);
					}
				}
			}

			fetch.onRequest(() => {
				if (first) {
					throw new Error();
				}
			});

			client.use(new RetryMiddleware());

			await client.getRaw('');
		});

	});

	describe('put()', () => {

		it('should call put', async () => {
			const body = {};
			const data = {
				message: 'hello world',
			};

			fetch.addResponse('PUT', '', data);
			fetch.onRequest((req: HttpRequestInterface) => {
				expect(req.body).to.be.equal(body);
			});

			const res = await client.putRaw('', body);

			expect(res.data).to.be.eql(data);
		});

	});

	describe('post()', () => {

		it('should call post', async () => {
			const body = {};
			const data = {
				message: 'hello world',
			};

			fetch.addResponse('POST', '', data);
			fetch.onRequest((req: HttpRequestInterface) => {
				expect(req.body).to.be.equal(body);
			});

			const res = await client.postRaw('', body);

			expect(res.data).to.be.eql(data);
		});

	});

	describe('delete()', () => {

		it('should call delete', async () => {
			const res = await client.delete('');

			expect(res.data).to.be.eql(undefined);
		});

	});

});

import 'reflect-metadata';
import {expect} from 'chai';

import {DecoratorMetadataLoader, Entity, SuperEntity, Id, Column, Relationship, SelfLink} from '../../../src';


let loader: DecoratorMetadataLoader;


describe('#MetadataLoaders/DecoratorLoader/DecoratorMetadataLoader', () => {

	beforeEach(() => {
		loader = new DecoratorMetadataLoader;
	});

	describe('getEntityMetadata()', () => {

		it('should return undefined if metadata is missing', () => {
			class TestClass {}

			expect(loader.getEntityMetadata(TestClass)).to.be.equal(undefined);
		});

		it('should return metadata', () => {
			const transformer = (data: any) => data;

			class TestRole {}

			@Entity()
			class TestClass
			{
				@Id() public readonly id: string;
				@Column({name: 'firstName'}) public readonly userFirstName: string;
				@Column({transformers: [transformer]}) public readonly lastName: string;
				@Relationship({name: 'role', entity: TestRole}) public readonly userRole: TestRole;
				@Relationship({entity: TestRole}) public readonly roleOld: TestRole;
				@SelfLink() public readonly link: string;
			}

			expect(loader.getEntityMetadata(TestClass)).to.be.eql({
				id: 'id',
				type: null,
				selfLink: 'link',
				columns: [
					{
						name: 'firstName',
						property: 'userFirstName',
						transformers: [],
					},
					{
						name: 'lastName',
						property: 'lastName',
						transformers: [transformer],
					},
				],
				relationships: [
					{
						name: 'role',
						property: 'userRole',
						entity: TestRole,
					},
					{
						name: 'roleOld',
						property: 'roleOld',
						entity: TestRole,
					},
				],
			});
		});

		it('should return different metadata for sibling classes', () => {
			class TestRel {}

			@SuperEntity()
			class BaseTestClass
			{
				@Id() public readonly id: string;
				@SelfLink() public readonly link: string;
				@Column() public readonly columnCommon: string;
				@Relationship({entity: TestRel}) public readonly relCommon: TestRel;
			}

			@Entity()
			class TestClassA extends BaseTestClass
			{
				@Column() public readonly columnA: string;
				@Relationship({entity: TestRel}) public readonly relA: TestRel;
			}

			@Entity()
			class TestClassB extends BaseTestClass
			{
				@Id() public readonly idB: string;
				@Column() public readonly columnB: string;
				@SelfLink() public readonly linkB: string;
				@Relationship({entity: TestRel}) public readonly relB: TestRel;
			}

			expect(loader.getEntityMetadata(TestClassA)).to.be.eql({
				id: 'id',
				type: null,
				selfLink: 'link',
				columns: [
					{
						name: 'columnA',
						property: 'columnA',
						transformers: [],
					},
					{
						name: 'columnCommon',
						property: 'columnCommon',
						transformers: [],
					},
				],
				relationships: [
					{
						name: 'relA',
						property: 'relA',
						entity: TestRel,
					},
					{
						name: 'relCommon',
						property: 'relCommon',
						entity: TestRel,
					},
				],
			});

			expect(loader.getEntityMetadata(TestClassB)).to.be.eql({
				id: 'idB',
				type: null,
				selfLink: 'linkB',
				columns: [
					{
						name: 'columnB',
						property: 'columnB',
						transformers: [],
					},
					{
						name: 'columnCommon',
						property: 'columnCommon',
						transformers: [],
					},
				],
				relationships: [
					{
						name: 'relB',
						property: 'relB',
						entity: TestRel,
					},
					{
						name: 'relCommon',
						property: 'relCommon',
						entity: TestRel,
					},
				],
			});
		});

	});

});

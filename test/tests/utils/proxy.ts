import {expect} from 'chai';

import {createProxy} from '../../../src/utils';


describe('#Utils/Proxy', () => {

	describe('createProxy()', () => {

		it('should create instance of class without calling constructor', () => {
			let called = false;

			class TestClass
			{
				constructor()
				{
					called = true;
				}
			}

			const proxy = createProxy(TestClass);

			expect(proxy).to.be.an.instanceOf(TestClass);
			expect(called).to.be.equal(false);
		});

	});

});

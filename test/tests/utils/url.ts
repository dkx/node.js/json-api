import {expect} from 'chai';

import {createUrl} from '../../../src/utils';


describe('#Utils/Url', () => {

	describe('createUrl()', () => {

		it('should create simple url', () => {
			expect(createUrl('', 'api')).to.be.equal('api');
		});

		it('should create url with parameters', () => {
			expect(createUrl('', 'api', {
				parameters: {
					a: 'A',
					b: 'B',
					c: 'C',
				},
			})).to.be.equal('api?a=A&b=B&c=C');
		});

		it('should create url with parameters from url and additional parameters', () => {
			expect(createUrl('', 'api?a=A', {
				parameters: {
					b: 'B',
					c: 'C',
				},
			})).to.be.equal('api?a=A&b=B&c=C');
		});

		it('should create complex url', () => {
			expect(createUrl('localhost:8080//', 'api?a=A', {
				includes: ['a', 'b.c.d'],
				parameters: {
					b: 'B',
					c: 'C',
				},
			})).to.be.equal('localhost:8080//api?a=A&b=B&c=C&include=a%2Cb.c.d');
		});

	});

});

import {FetchInterface} from '../../src/fetch';
import {HttpRequestInterface} from '../../src';


export declare type RequestEventCallback = (request: HttpRequestInterface) => void;

export class MockFetch implements FetchInterface
{
	private responses: {[request: string]: any} = {};

	private onRequestEvents: Array<RequestEventCallback> = [];

	public addResponse(method: string, url: string, data: any): void
	{
		this.responses[`${method.toUpperCase()}:${url}`] = data;
	}

	public onRequest(fn: RequestEventCallback): void
	{
		this.onRequestEvents.push(fn);
	}

	public async request<TResult>(request: HttpRequestInterface): Promise<TResult>
	{
		for (let event of this.onRequestEvents) {
			event(request);
		}

		return this.responses[`${request.method}:${request.url}`];
	}
}

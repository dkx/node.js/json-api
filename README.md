# DKX/JsonApi

Typescript entity mapper for json standard data (uses decorators).

## Implementations

* Angular: [@dkx/ng-json-api](https://gitlab.com/dkx/angular/json-api)

## Installation

```bash
$ npm install --save @dkx/json-api
``` 

or with yarn

```bash
$ yarn add @dkx/json-api
```

## Example

```typescript
import {ApiClient} from '@dkx/json-api';
import {User} from './user';

const api = new ApiClient(fetchImpl, {
    baseUrl: 'http://localhost:8080',
})

// get and map one user
const user = await api.getOne<User>(User, '/v1/users/42');

// get and map collection of users
const users = await api.getMany<User>(User, '/v1/users');
```

This library contains framework agnostic api client class for working with json api backend. The build in class does not
contain any HTTP implementation, it must be provided (`fetchImpl`).

Look at implementations at the top of this readme.

## Entities

Entity is type of class used to map json api data.

```typescript
import {Entity, Id, Column} from '@dkx/json-api';

@Entity()
class UserRole
{
    @Id() public id: string;
    @Column() public name: string;
}
```

Both `@Entity()` and `@Id()` decorators are mandatory for each entity class.

Class constructor is not called when data is mapped by this library.

## Entities inheritance

Entities can use inheritance too with a small tweak:

```typescript
import {SuperEntity, Entity} from '@dkx/json-api';

@SuperEntity()
class Parent {}

@Entity()
class Child extends Parent {}
```

### Column transformers

You can attach multiple transformer functions to every column. They are applied when entity is being mapped in the order 
they were setup.

```typescript
import {Entity, Id, Column} from '@dkx/json-api';

function timestampToDate(timestamp: number): Date
{
    return new Date(timestamp * 1000);
}

@Entity()
class UserRole
{
    @Id() public id: string;
    @Column() public name: string;
    @Column({transformers: [timestampToDate]}) public signedUpAt: Date;
}
```

## Relationships

Relationship joins two or more entities together.

```typescript
import {Entity, Id, Column, Relationship} from '@dkx/json-api';
import {UserRole} from './user-role';

@Entity()
class User
{
    @Id() public id: string;
    @Column() public email: string;
    @Relationship({entity: UserRole}) public role: UserRole;
}
```

### Cyclic dependencies

The code above will not work if two entities depends on each other (in dependency tree). Solution for this is to load 
entities for `@Relationship()` decorator lazily.

```typescript
import {Entity, Id, Column, Relationship, EntityRelationship} from '@dkx/json-api';
import {UserRole} from './user-role';

@Entity()
class User
{
    @Id() public id: string;
    @Column() public email: string;

    @Relationship({
        entity: import('./user-role').then((m) => m.UserRole),
    })
    public role: UserRole;
}
```

### Avoiding relationships

Relationships can be avoided altogether by using single purpose entities (defining them as DTO's).

```typescript
import {Entity, Id, Column} from '@dkx/json-api';

@Entity()
class UserDTO
{
    @Id() public id: string;
    @Column() public email: string;
    @Column({name: 'role.name'}) public roleName: string;
}
``` 

`role.name` in `@Column()` decorator is name of include in the json api data. This works also for arrays or nested 
includes.

## Middlewares

Example with retry middleware using [@dkx/retry-promise](https://www.npmjs.com/package/@dkx/retry-promise).

```typescript
import {MiddlewareInterface, MiddlewareHandlerInterface, HttpRequestInterface, HttpResponseInterface} from '@dkx/json-api';
import {retryPromise} from '@dkx/retry-promise';

const MAX_RETRIES: number = 5;
const DELAY: number = 100;

class RetryMiddleware implements MiddlewareInterface
{
    public invoke<TData>(request: HttpRequestInterface, next: MiddlewareHandlerInterface<TData>): Promise<HttpResponseInterface<TData>>
    {
        return retryPromise<HttpResponseInterface<TData>>(() => next.handle(request), {
            retries: MAX_RETRIES,
            delay: DELAY,
            exponential: true,
        });
    }
}

api.use(new RetryMiddleware());
```
